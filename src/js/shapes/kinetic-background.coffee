@KineticShapes = {} if not @KineticShapes

class KineticShapes.Background extends Kinetic.Shape
	constructor: (options) ->
		{@layer} = options

		super {
			x: 0
			y: 0			
			drawFunc: @drawFunc
			stroke: 'lightgray'
			strokeWidth: 1
		}

		console.log @layer
		@layer.add @
		@layer.draw()

		$(window).on 'resize', =>
			@layer.draw()

	drawFunc: (context) ->
		stage = @layer.getStage()

		p = 0
		bw = stage?.getWidth()
		bh = stage?.getHeight()

		@width = bw
		@height = bh


		for x in [0..bw+1] by 40
		 	context.moveTo(0.5 + x + p, p)
		 	context.lineTo(0.5 + x + p, bh + p)

		for x in [0..bh+1] by 40
		 	context.moveTo(p, 0.5 + x + p)
		 	context.lineTo(bw + p, 0.5 + x + p)

		context.fillStrokeShape(@)
	    # context.strokeStyle = "#00fefe"
	    


