@KineticShapes = {} if not @KineticShapes

class KineticShapes.WorkflowNode extends Kinetic.Group
	constructor: (options) ->
		{@modelScope, @layer} = options

		@width = 100
		@height = 50

		super {
			width: @width
			height: @height
			id: @modelScope.node.id
		}

		console.log @id()
		@.setAttrs {x, y} = @modelScope.node.position		
		
		# Initialize shapes
		@shapeRect = new Kinetic.Rect		
			width: @width
			height: @height
			fill: @modelScope.node.color
			stroke: 'black'
			strokeWidth: 1

		@newNode = new Kinetic.Rect
			width: 20
			height: 20
			fill: 'black'
			x: @width + 5
			y: (@height/2) - 10

		@shapeText = new Kinetic.Text
			text: @modelScope.node.name
			fill: 'black'
			align: 'center'
			width: @width
			y: 5

		@.add @newNode
		@.add @shapeRect
		@.add @shapeText
		@layer.add @

		@.setZIndex(1)
		@.setDraggable(true)

		scope = @modelScope

		@newNode.hide()

		@newNode.on 'mousedown', (event) ->
			console.log 'Add arrow', event
			# event.evt.stopPropagation()
			# event.evt.stopPropagation()
			event.cancelBubble = true;


		# Scope node events
		@modelScope.$watch 'node.name', (newValue, oldValue, scope) =>
			console.log 'test', newValue, oldValue, scope
			@shapeText.setText @modelScope.node.name

			@layer.draw()

		@modelScope.$watch 'node.selected', =>			
			if @modelScope.node.selected
				@shapeRect.setAttrs {stroke: 'red'}				
				@newNode.show()
			else			
				@shapeRect.setAttrs {stroke: 'black'}
				@newNode.hide()
			@layer.draw()

		@.on 'mouseover', =>
			@shapeRect.setAttrs 
				strokeWidth: 1
				shadowColor: 'black'
				shadowBlur: 100
				shadowOffset: {x:2, y:2}
				shadowOpacity: 0.2
			
			@layer.draw()
		@.on 'mouseout', =>
			@shapeRect.setAttrs 
				strokeWidth: 1
				shadowColor: 'black'
				shadowBlur: 0
				shadowOffset: {x:0, y:0}
				shadowOpacity: 0

			@layer.draw()

		@.on 'click', (evt) =>
			@modelScope.$apply =>
				@modelScope.node.selected = !@modelScope.node.selected
				@modelScope.$emit('childSelected', {'name': @modelScope.node.name})

		@.on 'dragstart', (evt) =>			
			@tween.pause() if @tween
			@.setAttrs
				shadowOffset:
					x: 15
					y: 15
				scale:
					x: 1.2
					y: 1.2

		@.on 'dragend', (evt) =>
			@modelScope.$apply =>
				@modelScope.node.selected = false
			@tween = new Kinetic.Tween
				node: @
				duration: 0.5
				easing: Kinetic.Easings.ElasticEaseOut
				scaleX: 1
				scaleY: 1
				shadowOffsetX: 5
				shadowOffsetY: 5

			@tween.play()
