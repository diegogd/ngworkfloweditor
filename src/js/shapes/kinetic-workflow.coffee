@KineticShapes = {} if not @KineticShapes

class KineticShapes.Workflow
	constructor: (options) ->
		{@stage, @scope} = options

		backgroundLayer = new Kinetic.Layer()
		background = new KineticShapes.Background
			layer: backgroundLayer

		nodesLayer = new Kinetic.Layer()

		@nodes = @scope.workflowModel.nodes
		@links = @scope.workflowModel.links

		for nodeModel in @nodes
			modelScope = @scope.$new()
			modelScope.node = nodeModel

			node = new KineticShapes.WorkflowNode
				modelScope: modelScope
				layer: nodesLayer


		@stage.add backgroundLayer
		@stage.add nodesLayer


		linksLayer = new Kinetic.Layer()

		for linkModel in @links
			linkModelScope = @scope.$new()
			linkModelScope.link = linkModel
			link = new KineticShapes.WorkflowLink
				modelScope: linkModelScope
				layer: nodesLayer #linksLayer
				nodesLayer: nodesLayer

		nodesLayer.on 'drag', ->
			linksLayer.draw()

		@stage.add linksLayer

		@stage.draw()

		@scope.$on 'childSelected', (event, args) =>
			for node in @nodes
				if node.name != args.name
					node.selected = false
				else
					node.name = node.name + '.'

