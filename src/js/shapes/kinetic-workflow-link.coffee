@KineticShapes = {} if not @KineticShapes

class KineticShapes.WorkflowLink extends Kinetic.Group
	constructor: (options) ->
		{@modelScope, @layer, @nodesLayer} = options

		super { x: 0, y: 0 }

		@layer.add @
		@.setZIndex(0)

		getCenterPosition = (item) ->
			position = item.getPosition()
			height = item.getHeight()
			width = item.getWidth()
			position.x += (width/2)
			position.y += (height/2)		
			position

		@lineShape = new Kinetic.Shape
			x: 0
			y: 0	
			stroke: 'red'
			drawFunc: (context) =>
				nodeFrom = @nodesLayer.find("##{@modelScope.link.fromNode}")
				if nodeFrom.length 
					@origin = getCenterPosition nodeFrom[0]

				nodeTo = @nodesLayer.find("##{@modelScope.link.toNode}")
				if nodeTo.length 
					@target = getCenterPosition nodeTo[0]

				context.beginPath()
				context.moveTo @origin.x, @origin.y
				context.lineTo @target.x, @target.y
				context.closePath()
				context.fillStrokeShape @lineShape
				@lineShape.moveToBottom()

		@add @lineShape


		
		
		