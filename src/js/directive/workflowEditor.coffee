angular.module 'ng-workflow-editor'
	.directive 'workflowEditor', ($timeout) ->
		restict: "A"
		scope: 
			'workflowModel': '='
		link: (scope, element) ->

			element.append '<div id="container-wf"></div>'			

			element = $(element).find("#container-wf")
			stage = new Kinetic.Stage
				container: element[0]
				width: 800,
				height: 600

			@workflow = new KineticShapes.Workflow
				stage: stage,
				scope: scope

			# Rescale Events on resize and after DOM load
			rescaleScope = ->
				stage.setAttrs
					width: $(element).width()
					height: 600
			$(window).on 'resize', rescaleScope
			$ rescaleScope



