module.exports = (grunt) ->
	os = require('os')
	iface = os.networkInterfaces()
	for details in iface['eth0']
		if details.family == 'IPv4'
			lookupIpAddress = details.address;

	console.log "Connect to http://#{lookupIpAddress}:8080"

	@initConfig
		pkg: grunt.file.readJSON('package.json')
		# Watch section
		watch:
			scripts_js:
				files: ['src/js/**/*.js']
				tasks: ['uglify']
			scripts_coffee:
				files: ['src/js/**/*.coffee']
				tasks: ['coffee']
			stylesheet:
				files: ['src/css/**/*.less']
				tasks: ['less']
			update_dist:
				files: ['dist/**/*']
				options:
					livereload: true

		# Coffee compiling
		coffee:
			compile: 
				files:
					"src/js/tmp/result.js": [
						"src/js/*.coffee"
						"src/js/**/*.coffee"
					]

		# Less task configuration
		less:
			compile:
				options: 
					paths: "src/css"				
				files:
					"dist/css/<%= pkg.name %>.css": 'src/css/*.less'

		# Uglify configuration
		uglify:
			options:
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
				sourceMap: true
			dist:
				files:
					"dist/js/<%= pkg.name %>.min.js": 'src/**/*.js'

		# Bower configuration
		bower:
			install: {}

		connect:
			server:
				options:
					port: 8080,
					hostname: '*'




	# Load Grunt plugins
	@loadNpmTasks "grunt-contrib-jshint"
	@loadNpmTasks "grunt-bower-task"
	@loadNpmTasks "grunt-contrib-watch"
	@loadNpmTasks "grunt-contrib-coffee"
	@loadNpmTasks "grunt-contrib-less"
	@loadNpmTasks "grunt-contrib-uglify"
	@loadNpmTasks "grunt-contrib-connect"
	@loadNpmTasks "grunt-notify"

	# Register tasks
	@registerTask "default", ["connect", "watch"]
	@registerTask "build", [""]

